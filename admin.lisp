(in-package :webapp)

(defparameter *admin-username* "marcos")
(defparameter *admin-password* "ukeleles")

(defmacro with-authorization (&body body)
  `(multiple-value-bind (username password)
       (hunchentoot:authorization)
     (if (and (equalp username *admin-username*)
              (equalp password *admin-password*))
         (progn
           ,@body)
         (hunchentoot:require-authorization "admin"))))

(defparameter +admin.html+ (djula:compile-template*
                            (princ-to-string
                             (merge-pathnames "admin.html" *templates-folder*))))

(define-easy-handler (admin :uri "/admin")
    ()
  (with-authorization
    (redirect "/admin/inicio")))

;; Admin welcome

(defparameter +admin-welcome.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-welcome.html" *templates-folder*))))

(define-easy-handler (admin/welcome :uri "/admin/inicio")
    ()
  (with-authorization
    (let ((welcome-page (get-welcome-page)))
      (with-output-to-string (s)
        (djula:render-template* +admin-welcome.html+ s
                                :title "Administracion"
                                :project-name "Administracion"
                                :welcome-page welcome-page
                                :mode "welcome"
                                :marketing (get-marketing)
                                :slides (get-slides)
                                :action "/admin/inicio/actualizar")))))

(define-easy-handler (admin/welcome/update :uri "/admin/inicio/actualizar"
                                           :default-request-type :post)
    ((marketing-title :parameter-type 'list)
     (marketing-description :parameter-type 'list))
  (with-authorization
    (update-welcome-page (post-parameter "title")
                         (post-parameter "content")
                         (post-parameter "message")
                         (post-parameter "button-msg"))
    (let ((marketing (loop
                        for title in marketing-title
                        for description in marketing-description
                        when (and title description)
                        collect (list :title title :description description))))
      (update-marketing marketing))
    (redirect "/admin/inicio")))

;; Admin contact

(defparameter +admin-contact.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-contact.html" *templates-folder*))))

(define-easy-handler (admin/contact :uri "/admin/contacto")
    ()
  (with-authorization
    (let ((contact-page (get-contact-page)))
      (with-output-to-string (s)
        (djula:render-template* +admin-contact.html+ s
                                :title "Administracion"
                                :project-name "Administracion"
                                :contact-page contact-page
                                :mode "contact"
                                :action "/admin/contacto/actualizar")))))

(define-easy-handler (admin/contact/update :uri "/admin/contacto/actualizar"
                                           :default-request-type :post)
    ()
  (with-authorization
    (update-contact-page (post-parameter "content"))
    (redirect "/admin/contacto")))

;; Admin process

(defparameter +admin-process.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-process.html" *templates-folder*))))

(define-easy-handler (admin/process :uri "/admin/proceso")
    ()
  (with-authorization
    (let ((process-page (get-process-page)))
      (with-output-to-string (s)
        (djula:render-template* +admin-process.html+ s
                                :title "Administracion"
                                :project-name "Administracion"
                                :process-page process-page
                                :mode "process"
                                :action "/admin/proceso/actualizar")))))

(define-easy-handler (admin/process/update :uri "/admin/proceso/actualizar"
                                           :default-request-type :post)
    ()
  (with-authorization
    (update-process-page (post-parameter "content"))
    (redirect "/admin/proceso")))

;; Process pictures

(defparameter +admin-create-process-picture.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-create-process-picture.html" *templates-folder*))))

(define-easy-handler (admin/process-picture/new :uri "/admin/imagenes-proceso/nueva"
                                                :default-request-type :get)
    ()
  (with-authorization
    (with-output-to-string (s)
      (djula:render-template* +admin-create-process-picture.html+ s
                              :title "Administracion"
                              :project-name "Administracion"
                              :mode "process"
                              :action "/admin/imagenes-proceso/crear"))))

(define-easy-handler (admin/process-picture/create
                      :uri "/admin/imagenes-proceso/crear"
                      :default-request-type :post)
    ()
  (with-authorization
    (add-process-picture :title (post-parameter "title")
                         :description (post-parameter "description")
                         :date (or (chronicity:parse (post-parameter "date"))
                                   (local-time:now))
                         :filename (handle-picture-file (post-parameter "file")))
    (redirect "/admin/proceso")))

(defparameter +admin-edit-process-picture.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-edit-process-picture.html" *templates-folder*))))

(define-easy-handler (admin/process-picture/edit
                      :uri "/admin/imagenes-proceso"
                      :default-request-type :get)
    (imagen)
  (with-authorization
    (let ((picture (find-picture (get-process-page) (parse-integer imagen))))
      (if (not picture)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Imagen no encontrada")
          ;; else
          (with-output-to-string (s)
            (djula:render-template* +admin-edit-process-picture.html+ s
                                    :title "Administracion"
                                    :project-name "Administracion"
                                    :mode "process"
                                    :picture picture
                                    :action "/admin/imagenes-proceso/actualizar"))))))

(define-easy-handler (admin/process-picture/update
                      :uri "/admin/imagenes-proceso/actualizar"
                      :default-request-type :post)
    ()
  (with-authorization
    (let ((picture (find-picture (get-process-page)
                                 (parse-integer (post-parameter "picture-id")))))
      (if (not picture)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Imagen no encontrada")
          ;; else
          (progn
            (setf (title picture) (post-parameter "title"))
            (setf (description picture) (post-parameter "description"))
            (setf (date picture) (or (chronicity:parse (post-parameter "date"))
                                     (local-time:now)))
            (update-picture picture)
            (redirect (format nil "/admin/proceso")))))))

(define-easy-handler (admin/process-picture/delete
                      :uri "/admin/imagenes-proceso/borrar"
                      :default-request-type :get)
    (id)
  (with-authorization
    (let* ((process (get-process-page))
           (picture (find-picture process (parse-integer id) )))
      (if (not picture)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Imagen no encontrada")
          ;; else
          (progn
            (delete-process-picture picture)
            (redirect "/admin/proceso"))))))

;; Admin works

(defparameter +admin-works.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-works.html" *templates-folder*))))

(define-easy-handler (admin/works :uri "/admin/trabajos")
    ()
  (with-authorization
    (let ((works (get-works)))
      (with-output-to-string (s)
        (djula:render-template* +admin-works.html+ s
                                :title "Administracion"
                                :project-name "Administracion"
                                :mode "works"
                                :works works)))))

;; Update

(defparameter +admin-work.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-work.html" *templates-folder*))))

(defun parse-admin/work-uri (uri)
  (cl-ppcre:register-groups-bind (id) ("^/admin/trabajos/(\\d+)$" uri)
    (when id
      (parse-integer id))))

(defun admin/work-uri (request)
  (not (null (parse-admin/work-uri (request-uri request)))))

(define-easy-handler (admin/work :uri 'admin/work-uri
                                 :default-request-type :get)
    ()
  (with-authorization
    (let ((work (find-work (parse-admin/work-uri (request-uri*)))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          ;; else
          (with-output-to-string (s)
            (djula:render-template* +admin-work.html+ s
                                    :title "Administracion"
                                    :project-name "Administracion"
                                    :mode "works"
                                    :action "/admin/trabajos/actualizar"
                                    :work work))))))

(define-easy-handler (admin/work/update :uri "/admin/trabajos/actualizar"
                                        :default-request-type :post)
    ()
  (with-authorization
    (let ((work (find-work (parse-integer (post-parameter "id")))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          ;; else
          (progn
            (setf (title work) (post-parameter "title"))
            (setf (description work) (post-parameter "description"))
            (setf (short-description work) (post-parameter "short-description"))
            (setf (date work) (or (chronicity:parse (post-parameter "date"))
                                  (local-time:now)))
            (setf (publish work) (post-parameter "publish"))

            (update-work work)

            (redirect (format nil "/admin/trabajos/~A" (id work))))))))

;; Create

(defparameter +admin-create-work.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-create-work.html" *templates-folder*))))

(define-easy-handler (admin/work/new :uri "/admin/trabajos/nuevo"
                                     :default-request-type :get)
    ()
  (with-authorization
    (with-output-to-string (s)
      (djula:render-template* +admin-create-work.html+ s
                              :title "Administracion"
                              :project-name "Administracion"
                              :mode "works"
                              :action "/admin/trabajos/crear"))))

(define-easy-handler (admin/work/create :uri "/admin/trabajos/crear"
                                        :default-request-type :post)
    ()
  (with-authorization
    (let ((work (create-work :title (post-parameter "title")
                             :description (post-parameter "description")
                             :short-description (post-parameter "short-description")
                             :date (or (chronicity:parse (post-parameter "date"))
                                       (local-time:now)))))
      (redirect (format nil "/admin/trabajos/~A" (id work))))))

(define-easy-handler (admin/work/delete :uri "/admin/trabajos/borrar"
                                        :default-request-type :get)
    (id)
  (with-authorization
    (let ((work (find-work (parse-integer id))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          ;; else
          (progn
            (delete-work work)
            (redirect "/admin/trabajos"))))))

;; Pictures

;; Create

(defparameter +admin-create-picture.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-create-picture.html" *templates-folder*))))

(define-easy-handler (admin/picture/new :uri "/admin/imagenes/nueva"
                                        :default-request-type :get)
    (trabajo)
  (with-authorization
    (let ((work (find-work (parse-integer trabajo))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          ;; else
          (with-output-to-string (s)
            (djula:render-template* +admin-create-picture.html+ s
                                    :title "Administracion"
                                    :project-name "Administracion"
                                    :mode "works"
                                    :work work
                                    :action "/admin/imagenes/crear"))))))

(defun handle-picture-file (post-parameter)
  (when (and post-parameter
             (listp post-parameter))
    (destructuring-bind (path file-name content-type)
        post-parameter
      (let ((new-filename (cl-ppcre:register-groups-bind (filename extension)
                              ("(.*)\\.(.*)" file-name)
                            (format nil "~A.~A"
                                    (ironclad:byte-array-to-hex-string (md5:md5sum-file path))
                                    extension))))
        (let ((new-path (make-pathname :name new-filename
                                       :type nil
                                       :defaults *pictures-directory*)))
          ;; strip directory info sent by Windows browsers
          (when (search "Windows" (user-agent) :test 'char-equal)
            (setq file-name (cl-ppcre:regex-replace ".*\\\\" file-name "")))
          (rename-file path (ensure-directories-exist new-path))
          new-filename)))))

(define-easy-handler (admin/picture/create
                      :uri "/admin/imagenes/crear"
                      :default-request-type :post)
    ()
  (with-authorization
    (let ((work (find-work (parse-integer (post-parameter "work")))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          ;; else
          (let ((picture (create-picture :title (post-parameter "title")
                                         :description (post-parameter "description")
                                         :filename (handle-picture-file (post-parameter "file"))
                                         :date (or (chronicity:parse (post-parameter "date"))
                                                   (local-time:now)))))
            (add-picture work picture)
            (redirect (format nil "/admin/trabajos/~A" (id work))))))))

(defparameter +admin-edit-picture.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-edit-picture.html" *templates-folder*))))

(define-easy-handler (admin/picture/edit :uri "/admin/imagenes"
                                         :default-request-type :get)
    (trabajo imagen)
  (with-authorization
    (let ((work (find-work (parse-integer trabajo))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          (let ((picture (find-picture work (parse-integer imagen))))
            (if (not picture)
                (progn
                  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
                  "Imagen no encontrada")
                ;; else
                (with-output-to-string (s)
                  (djula:render-template* +admin-edit-picture.html+ s
                                          :title "Administracion"
                                          :project-name "Administracion"
                                          :mode "works"
                                          :work work
                                          :picture picture
                                          :action "/admin/imagenes/actualizar"))))))))

(define-easy-handler (admin/picture/update
                      :uri "/admin/imagenes/actualizar"
                      :default-request-type :post)
    ()
  (with-authorization
    (let ((work (find-work (parse-integer (post-parameter "work-id")))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          (let ((picture (find-picture work (parse-integer (post-parameter "picture-id")))))
            (if (not picture)
                (progn
                  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
                  "Imagen no encontrada")
                ;; else
                (progn
                  (setf (title picture) (post-parameter "title"))
                  (setf (description picture) (post-parameter "description"))
                  (setf (date picture) (or (chronicity:parse (post-parameter "date"))
                                           (local-time:now)))
                  (update-picture picture)
                  (redirect (format nil "/admin/trabajos/~A" (id work))))))))))

(define-easy-handler (admin/picture/delete
                      :uri "/admin/imagenes/borrar"
                      :default-request-type :get)
    (work-id picture-id)
  (with-authorization
    (let ((work (find-work (parse-integer work-id))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          (let ((picture (find-picture work (parse-integer picture-id))))
            (if (not picture)
                (progn
                  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
                  "Imagen no encontrada")
                ;; else
                (progn
                  (delete-picture work picture)
                  (redirect (format nil "/admin/trabajos/~A" (id work))))))))))

(define-easy-handler (admin/picture/move
                      :uri "/admin/imagenes/mover"
                      :default-request-type :get)
    (work-id picture-id direction)
  (with-authorization
    (let ((work (find-work (parse-integer work-id))))
      (if (not work)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Trabajo no encontrado")
          (let ((picture (find-picture work (parse-integer picture-id))))
            (if (not picture)
                (progn
                  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
                  "Imagen no encontrada")
                (progn
                  (move-picture work picture (intern (string-upcase direction) :keyword))
                  (redirect (format nil "/admin/trabajos/~A" (id work))))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Admin models
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter +admin-models.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-models.html" *templates-folder*))))

(define-easy-handler (admin/models :uri "/admin/modelos")
    ()
  (with-authorization
    (let ((models (get-models)))
      (with-output-to-string (s)
        (djula:render-template* +admin-models.html+ s
                                :title "Administracion"
                                :project-name "Administracion"
                                :mode "models"
                                :models models)))))

;; Update

(defparameter +admin-model.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-model.html" *templates-folder*))))

(defun parse-admin/model-uri (uri)
  (cl-ppcre:register-groups-bind (id) ("^/admin/modelos/(\\d+)$" uri)
    (when id
      (parse-integer id))))

(defun admin/model-uri (request)
  (not (null (parse-admin/model-uri (request-uri request)))))

(define-easy-handler (admin/model :uri 'admin/model-uri
                                  :default-request-type :get)
    ()
  (with-authorization
    (let ((model (find-model (parse-admin/model-uri (request-uri*)))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          ;; else
          (with-output-to-string (s)
            (djula:render-template* +admin-model.html+ s
                                    :title "Administracion"
                                    :project-name "Administracion"
                                    :mode "models"
                                    :action "/admin/modelos/actualizar"
                                    :model model))))))

(define-easy-handler (admin/model/update :uri "/admin/modelos/actualizar"
                                         :default-request-type :post)
    ()
  (with-authorization
    (let ((model (find-model (parse-integer (post-parameter "id")))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          ;; else
          (progn
            (setf (title model) (post-parameter "title"))
            (setf (description model) (post-parameter "description"))
            (setf (short-description model) (post-parameter "short-description"))
            (setf (date model) (or (chronicity:parse (post-parameter "date"))
                                   (local-time:now)))
            (setf (publish model) (post-parameter "publish"))

            (update-model model)

            (redirect (format nil "/admin/modelos/~A" (id model))))))))

;; Create

(defparameter +admin-create-model.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-create-model.html" *templates-folder*))))

(define-easy-handler (admin/model/new :uri "/admin/modelos/nuevo"
                                      :default-request-type :get)
    ()
  (with-authorization
    (with-output-to-string (s)
      (djula:render-template* +admin-create-model.html+ s
                              :title "Administracion"
                              :project-name "Administracion"
                              :mode "models"
                              :action "/admin/modelos/crear"))))

(define-easy-handler (admin/model/create :uri "/admin/modelos/crear"
                                         :default-request-type :post)
    ()
  (with-authorization
    (let ((model (create-model :title (post-parameter "title")
                               :description (post-parameter "description")
                               :short-description (post-parameter "short-description")
                               :date (or (chronicity:parse (post-parameter "date"))
                                         (local-time:now)))))
      (redirect (format nil "/admin/modelos/~A" (id model))))))

(define-easy-handler (admin/model/delete :uri "/admin/modelos/borrar"
                                         :default-request-type :get)
    (id)
  (with-authorization
    (let ((model (find-model (parse-integer id))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          ;; else
          (progn
            (delete-model model)
            (redirect "/admin/modelos"))))))

;; Pictures

;; Create

(defparameter +admin-create-model-picture.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-create-model-picture.html" *templates-folder*))))

(define-easy-handler (admin/model-picture/new :uri "/admin/imagenes-modelo/nueva"
                                              :default-request-type :get)
    (modelo)
  (with-authorization
    (let ((model (find-model (parse-integer modelo))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          ;; else
          (with-output-to-string (s)
            (djula:render-template* +admin-create-model-picture.html+ s
                                    :title "Administracion"
                                    :project-name "Administracion"
                                    :mode "models"
                                    :model model
                                    :action "/admin/imagenes-modelo/crear"))))))

(define-easy-handler (admin/model-picture/create
                      :uri "/admin/imagenes-modelo/crear"
                      :default-request-type :post)
    ()
  (with-authorization
    (let ((model (find-model (parse-integer (post-parameter "model")))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          ;; else
          (let ((picture (create-picture :title (post-parameter "title")
                                         :description (post-parameter "description")
                                         :date (or (chronicity:parse (post-parameter "date"))
                                                   (local-time:now))
                                         :filename (handle-picture-file (post-parameter "file")))))
            (add-picture-to-model model picture)
            (redirect (format nil "/admin/modelos/~A" (id model))))))))

(defparameter +admin-edit-model-picture.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-edit-model-picture.html" *templates-folder*))))

(define-easy-handler (admin/model-picture/edit
                      :uri "/admin/imagenes-modelo"
                      :default-request-type :get)
    (modelo imagen)
  (with-authorization
    (let ((model (find-model (parse-integer modelo))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          (let ((picture (find-picture model (parse-integer imagen))))
            (if (not picture)
                (progn
                  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
                  "Imagen no encontrada")
                ;; else
                (with-output-to-string (s)
                  (djula:render-template* +admin-edit-model-picture.html+ s
                                          :title "Administracion"
                                          :project-name "Administracion"
                                          :mode "models"
                                          :model model
                                          :picture picture
                                          :action "/admin/imagenes-modelo/actualizar"))))))))

(define-easy-handler (admin/model-picture/update
                      :uri "/admin/imagenes-modelo/actualizar"
                      :default-request-type :post)
    ()
  (with-authorization
    (let ((model (find-model (parse-integer (post-parameter "model-id")))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          (let ((picture (find-picture model (parse-integer (post-parameter "picture-id")))))
            (if (not picture)
                (progn
                  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
                  "Imagen no encontrada")
                ;; else
                (progn
                  (setf (title picture) (post-parameter "title"))
                  (setf (description picture) (post-parameter "description"))
                  (setf (date picture) (or (chronicity:parse (post-parameter "date"))
                                           (local-time:now)))
                  (update-picture picture)
                  (redirect (format nil "/admin/modelos/~A" (id model))))))))))

(define-easy-handler (admin/model-picture/delete
                      :uri "/admin/imagenes-modelo/borrar"
                      :default-request-type :get)
    (model-id picture-id)
  (with-authorization
    (let ((model (find-model (parse-integer model-id))))
      (if (not model)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Modelo no encontrado")
          (let ((picture (find-picture model (parse-integer picture-id) )))
            (if (not picture)
                (progn
                  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
                  "Imagen no encontrada")
                ;; else
                (progn
                  (delete-picture-from-model model picture)
                  (redirect (format nil "/admin/modelos/~A" (id model))))))))))

(defun render-marketing-admin ()
  (let ((marketing (get-marketing)))
    (cl-who:with-html-output-to-string (html)
      (loop for i from 0 to 5
         do
           (htm
            (:div :class "form-group"
                  (:div :class "col-md-3"
                        (:input :name "marketing-title" :placeholder "Titulo de marketing"
                                :class "form-control input-md" :type "text"
                                :value (getf (nth i marketing) :title)))
                  (:div :class "col-md-6"
                        (:input :name "marketing-description" :placeholder "Descripcion de marketing"
                                :class "form-control input-md" :type "test"
                                :value (getf (nth i marketing) :description)))))))))

;; Slides

;; Create

(defparameter +admin-create-slide.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-create-slide.html" *templates-folder*))))

(define-easy-handler (admin/slides/new :uri "/admin/slides/nueva"
                                       :default-request-type :get)
    ()
  (with-authorization
    (with-output-to-string (s)
      (djula:render-template* +admin-create-slide.html+ s
                              :title "Administracion"
                              :project-name "Administracion"
                              :mode "welcome"
                              :action "/admin/slides/crear"))))

(define-easy-handler (admin/slides/create
                      :uri "/admin/slides/crear"
                      :default-request-type :post)
    ()
  (with-authorization
    (let ((slide (add-slide-picture
                  :title (post-parameter "title")
                  :description (post-parameter "description")
                  :filename (handle-picture-file (post-parameter "file")))))
      (redirect (format nil "/admin/inicio")))))

(defparameter +admin-edit-slide.html+
  (djula:compile-template*
   (princ-to-string
    (merge-pathnames "admin-edit-slide.html" *templates-folder*))))

(define-easy-handler (admin/slides/edit :uri "/admin/slides/editar"
                                        :default-request-type :get)
    (id)
  (with-authorization
    (with-output-to-string (s)
      (djula:render-template* +admin-edit-slide.html+ s
                              :title "Administracion"
                              :project-name "Administracion"
                              :mode "welcome"
                              :slide (find-slide (parse-integer id))
                              :action "/admin/slides/actualizar"))))

(define-easy-handler (admin/slides/update
                      :uri "/admin/slides/actualizar"
                      :default-request-type :post)
    ()
  (with-authorization
    (let ((slide (find-slide (parse-integer (post-parameter "slide")))))
      (if (not slide)
          (progn
            (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
            "Slide no encontrada")
          (progn
            (setf (title slide) (post-parameter "title"))
            (setf (description slide) (post-parameter "description"))
            (setf (date slide) (or (chronicity:parse (post-parameter "date"))
                                   (local-time:now)))
            (update-picture slide)
            (redirect (format nil "/admin/inicio")))))))

(define-easy-handler (admin/slides/delete
                      :uri "/admin/slides/borrar"
                      :default-request-type :get)
    (id)
  (with-authorization
    (let ((slide (find-slide (parse-integer id))))
      (delete-slide-picture slide)
      (redirect (format nil "/admin/inicio")))))

;; Background image upload

(define-easy-handler (upload-background-image :uri "/upload-background-image"
                                              :default-request-type :post)
    ()
  (destructuring-bind (path filename content-type)
      (post-parameter "file")
    (let ((image-dir
           (if (getenv "OPENSHIFT_DATA_DIR")
               (ensure-directories-exist (merge-pathnames #p"data/"
                                                          (pathname (getenv "OPENSHIFT_DATA_DIR"))))
               (ensure-directories-exist (asdf:system-relative-pathname :webapp "data/")))))
      (let ((background-image-pathname
             (merge-pathnames filename image-dir)))
        (sb-ext:run-program "/bin/mv"
                            (list (princ-to-string path)
                                  (princ-to-string background-image-pathname)))
        (update-background-image :pathname background-image-pathname
                                 :content-type content-type
                                 :uploaded-filename filename)
        (hunchentoot:redirect "/admin")))))

;; HTML editor images uploader

(defparameter *images-directory*
  (if (getenv "OPENSHIFT_DATA_DIR")
      (ensure-directories-exist (merge-pathnames #p"images/"
                                                 (pathname (getenv "OPENSHIFT_DATA_DIR"))))
      (ensure-directories-exist (asdf:system-relative-pathname :webapp "images/")))
  "Images upload directory")

(push (hunchentoot:create-folder-dispatcher-and-handler
       "/admin/images/"
       *images-directory*)
      hunchentoot:*dispatch-table*)

(define-easy-handler (upload-image :uri "/admin/uploadImage"
                                   :default-request-type :post)
    ((callback :real-name "CKEditorFuncNum" :request-type :get)
     (ckeditor :real-name "CKEditor" :request-type :get)
     (lang-code :real-name "langCode" :request-type :get))
  (destructuring-bind (path filename content-type)
      (post-parameter "upload")
    (let ((filename (format nil "~A-~A" (get-universal-time) filename)))
      (let ((image-pathname
	     (merge-pathnames filename *images-directory*))
	    (image-url
             (format nil "/admin/images/~A" filename)))
	(sb-ext:run-program "/bin/mv"
			    (list (princ-to-string path)
				  (princ-to-string image-pathname)))
        (format nil "<html><body><script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(~A, \"~A\",\"~A\");</script></body></html>"
                callback
                image-url
                "")))))
