;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: WEBAPP; Base: 10 -*-
;;;
;;; Copyright (C) 2012  Anthony Green <green@spindazzle.org>
;;;                         
;;; Webapp is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3, or (at your
;;; option) any later version.
;;;
;;; Webapp is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Webapp; see the file COPYING3.  If not see
;;; <http://www.gnu.org/licenses/>.

;; Top level for webapp

(in-package :webapp)

;; webapp can be run within the openshift environment, as well as
;; interactively on your desktop.  Within the openshift environment,
;; we bind to OPENSHIFT_INTERNAL_IP and OPENSHIFT_INTERNAL_PORT.
;; Non-openshift deployments will bind to the following address and
;; port:

(defvar *default-ip-string* "localhost")
(defvar *default-port-string* "9090")

;; Our server....

(defvar *hunchentoot-server* nil)

(defun getenv (name &optional default)
  "Portable getenv function"
  #+CMU
  (let ((x (assoc name ext:*environment-list*
		  :test #'string=)))
    (if x (cdr x) default))
  #-CMU
  (or
   #+Allegro (sys:getenv name)
   #+CLISP (ext:getenv name)
   #+ECL (si:getenv name)
   #+SBCL (sb-ext:posix-getenv name)
   #+LISPWORKS (lispworks:environment-variable name)
   #+ABCL(java:jstatic "getenv" (java:jclass "java.lang.System") name)
   default))

;; Start the web app.

(defun start-webapp (&rest interactive)
  "Start the web application and have the main thread sleep forever,
  unless INTERACTIVE is non-nil."

  ;; Start prevalence store
  (init-ukelele-system)

  ;; If in openshift, change hunchentoot temporal directory
  ;; for file upload to work
  (when (getenv "OPENSHIFT_DATA_DIR")
    (setf hunchentoot:*tmp-directory*
	  (ensure-directories-exist
	   (format nil "~Atmp/"
		   (getenv "OPENSHIFT_DATA_DIR")))))
		   
  ;; Start hunchentoot
  (let ((openshift-ip   (getenv "OPENSHIFT_DIY_IP"))
 	(openshift-port (getenv "OPENSHIFT_DIY_PORT")))
    (let ((ip (if openshift-ip openshift-ip *default-ip-string*))
	  (port (if openshift-port openshift-port *default-port-string*)))
      (format t "** Starting hunchentoot @ ~A:~A~%" ip port)
      (setq *hunchentoot-server* (hunchentoot:start 
				  (make-instance 'hunchentoot:easy-acceptor 
						 :address ip
						 :port (parse-integer port))))
      (if (not interactive)
	  (loop
	   (sleep 3000))))))

(defun stop-webapp ()
  "Stop the web application."
  (stop-ukelele-system)
  (hunchentoot:stop *hunchentoot-server*))
