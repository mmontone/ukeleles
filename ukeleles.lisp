(in-package :webapp)

(defparameter *pictures-directory*
  (if (getenv "OPENSHIFT_DATA_DIR")
      (ensure-directories-exist (merge-pathnames #p"pictures/"
						 (pathname (getenv "OPENSHIFT_DATA_DIR"))))
      ;;else
      (ensure-directories-exist
       (asdf:system-relative-pathname :webapp "static/pictures/"))))

(defmethod uri ((picture picture))
  (format nil "/pict/~A" (filename picture)))

;; Localization

(cl-locale:define-dictionary ukeleles
  (:en (asdf:system-relative-pathname :webapp "i18n/en/message.lisp"))
  (:es (asdf:system-relative-pathname :webapp "i18n/es/message.lisp")))

(setf djula:*current-language*
      (if (equalp (getenv "OPENSHIFT_APP_NAME") "enukeleles")
	  :en
	  :es))

;; Statics
(defparameter *statics-dispatcher-and-handler*
  (create-folder-dispatcher-and-handler "/static/"
					(asdf:system-relative-pathname :webapp "static/")))

(push *statics-dispatcher-and-handler* hunchentoot:*dispatch-table*)

;; Zoho mail validation
(defparameter *zoho-mail-dispatcher-and-handler*
  (create-folder-dispatcher-and-handler "/zohoverify/"
					(asdf:system-relative-pathname :webapp "zohoverify/")))

(push *zoho-mail-dispatcher-and-handler* hunchentoot:*dispatch-table*)


;; Pictures
(defparameter *pictures-dispatcher-and-handler*
  (create-folder-dispatcher-and-handler "/pict/"
					*pictures-directory*))

(push *pictures-dispatcher-and-handler* hunchentoot:*dispatch-table*)

;; Templates

(setf djula::*djula-execute-package* (find-package :webapp))

(defparameter *templates-folder*
  (asdf:system-relative-pathname "webapp" "templates/"))

(defparameter +base.html+ (djula:compile-template*
			   (princ-to-string
			    (merge-pathnames "base.html" *templates-folder*))))

(defparameter +welcome.html+ (djula:compile-template*
			      (princ-to-string
			       (merge-pathnames "welcome.html" *templates-folder*))))

(defparameter +contact.html+ (djula:compile-template*
			      (princ-to-string
			       (merge-pathnames "contact.html" *templates-folder*))))

(defparameter +process.html+ (djula:compile-template*
			      (princ-to-string
			       (merge-pathnames "process.html" *templates-folder*))))

(defparameter +work.html+ (djula:compile-template*
			   (princ-to-string
			    (merge-pathnames "work.html" *templates-folder*))))

(defparameter +works.html+ (djula:compile-template*
			   (princ-to-string
			    (merge-pathnames "works.html" *templates-folder*))))

(defparameter +model.html+ (djula:compile-template*
			   (princ-to-string
			    (merge-pathnames "model.html" *templates-folder*))))

(defparameter +models.html+ (djula:compile-template*
			   (princ-to-string
			    (merge-pathnames "models.html" *templates-folder*))))


;; Root page
(define-easy-handler (root-handler :uri "/") ()
  (redirect "/inicio"))

;; Welcome page
(define-easy-handler (welcome-handler :uri "/inicio") ()
  (let ((welcome (get-welcome-page)))
    (with-output-to-string (s)
      (djula:render-template* +welcome.html+ s
			      :title "Ukeleles"
			      :project-name "Ukeleles"
			      :welcome welcome
			      :slides (get-slides)
			      :mode "welcome"))))

;; Contact page
(define-easy-handler (contact-handler :uri "/contacto") ()
  (let ((contact (get-contact-page)))
    (with-output-to-string (s)
      (djula:render-template* +contact.html+ s
			      :title "Ukeleles"
			      :project-name "Ukeleles"
			      :contact contact
			      :mode "contact"))))

;; Process page
(define-easy-handler (process-handler :uri "/proceso") ()
  (let ((process-page (get-process-page)))
    (with-output-to-string (s)
      (djula:render-template* +process.html+ s
			      :title "Ukeleles"
			      :project-name "Ukeleles"
			      :process-page process-page
			      :mode "process"))))

;; Works pages

;; Works

(define-easy-handler (works-handler :uri "/trabajos")
    ()
  (let ((works (get-published-works)))
    (with-output-to-string (s)
      (djula:render-template* +works.html+ s
			      :title "Ukekeles"
			      :project-name "Ukeleles"
			      :works works
			      :mode "works"))))			      
;; Single work
(defun parse-work-uri (uri)
  (cl-ppcre:register-groups-bind (id) ("^/trabajos/(\\d+)$" uri)
    (when id
      (parse-integer id))))

(defun work-uri (request)
  (not (null (parse-work-uri (request-uri request)))))

(define-easy-handler (work-handler :uri 'work-uri) ()
  (let ((work (find-work (parse-work-uri (request-uri*)))))
    (if (not work)
	(progn
	  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
	  "Trabajo no encontrado")
	;; else
	(with-output-to-string (s)
	  (djula:render-template* +work.html+ s
				  :title "Ukeleles"
				  :project-name "Ukeleles"
				  :work work
				  :mode "works")))))

;; Models pages

;; Models

(define-easy-handler (models-handler :uri "/modelos")
    ()
  (let ((models (get-published-models)))
    (with-output-to-string (s)
      (djula:render-template* +models.html+ s
			      :title "Ukekeles"
			      :project-name "Ukeleles"
			      :models models
			      :mode "models"))))			      
;; Single model
(defun parse-model-uri (uri)
  (cl-ppcre:register-groups-bind (id) ("^/modelos/(\\d+)$" uri)
    (when id
      (parse-integer id))))

(defun model-uri (request)
  (not (null (parse-model-uri (request-uri request)))))

(define-easy-handler (model-handler :uri 'model-uri) ()
  (let ((model (find-model (parse-model-uri (request-uri*)))))
    (if (not model)
	(progn
	  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
	  "Modelo no encontrado")
	;; else
	(with-output-to-string (s)
	  (djula:render-template* +model.html+ s
				  :title "Ukeleles"
				  :project-name "Ukeleles"
				  :model model
				  :mode "models")))))

(defun render-marketing (marketing)
  (cl-who:with-html-output-to-string (html)
    (loop for elem = (pop marketing)
       while elem do
	 (cl-who:with-html-output (html)
	   (:div :class "row"
		 (:div :class "col-lg-4"
		       (:h4 (str (getf elem :title)))
		       (:p (str (getf elem :description))))
		 (loop for i from 1 to 2
		    for elem = (pop marketing)
		    while elem
		    do
		      (cl-who:with-html-output (html)
			(:div :class "col-lg-4"
			      (:h4 (str (getf elem :title)))
			      (:p (str (getf elem :description)))))))))))

;; Background
(define-easy-handler (background-image :uri "/background-image") ()
  (let ((background-image (get-background-image)))
    (when background-image
      (hunchentoot:handle-static-file (background-image-pathname background-image)
				      (content-type background-image)))))
