(in-package :webapp)

(defmethod id (object)
  (cl-prevalence:get-id object))

(defparameter +date-format+
  '((:day 2) #\/ (:month 2) #\/ (:year 4) #\  :hour12 #\: :min #\  :ampm))

(defun print-date (date)
  (string-upcase 
   (local-time:format-timestring nil date
				 :format
				 +date-format+)))
;; Model

(defclass welcome-page ()
  ((title :initarg :title
	  :accessor title
	  :initform nil)
   (content :initarg :content
	    :accessor content
	    :initform nil)
   (message :initarg :message
	    :accessor message
	    :initform nil)
   (button-msg :initarg :button-msg
	       :accessor button-msg
	       :initform nil)))

(defclass contact-page ()
  ((content :initarg :content
	    :accessor content)))

(defclass process-page ()
  ((content :initarg :content
	    :accessor content
	    :initform nil)
   (pictures :initarg :pictures
	     :accessor pictures
	     :initform nil)))

(defclass work (cl-prevalence:object-with-id)
  ((title :initarg :title
	  :accessor title)
   (date :initarg date
	 :accessor date
	 :initform (local-time:now))
   (description :initarg :description
		:accessor description)
   (short-description :initarg :short-description
		      :accessor short-description
		      :initform nil)
   (pictures :initarg :pictures
	     :accessor pictures
	     :initform nil)
   (publish :initarg :publish
	    :accessor publish
	    :initform t)))

(defmethod print-object ((work work) stream)
  (print-unreadable-object (work stream :type t :identity t)
    (format stream "~A: ~S (~A)"
	    (id work)
	    (title work)
	    (if (publish work)
		"published"
		"not published"))))

(defun timestamp< (t1 t2)
  (cond
    ((not (typep t1 'local-time:timestamp))
     t2)
    ((not (typep t2 'local-time:timestamp))
     t1)
    (t
     (local-time:timestamp< t1 t2))))

(defmethod pictures ((work work))
  (sort (copy-list (slot-value work 'pictures))
	#'timestamp<
	:key #'date))

(defclass model (cl-prevalence:object-with-id)
  ((title :initarg :title
	  :accessor title)
   (date :initarg date
	 :accessor date
	 :initform (local-time:now))
   (description :initarg :description
		:accessor description)
   (short-description :initarg :short-description
		      :accessor short-description
		      :initform nil)
   (pictures :initarg :pictures
	     :accessor pictures
	     :initform nil)
   (publish :initarg :publish
	    :accessor publish
	    :initform t)))

(defmethod print-object ((model model) stream)
  (print-unreadable-object (model stream :type t :identity t)
    (format stream "~A: ~S (~A)"
	    (id model)
	    (title model)
	    (if (publish model)
		"published"
		"not published"))))

(defmethod pictures ((model model))
  (sort (copy-list (slot-value model 'pictures))
	#'timestamp<
	:key #'date))

(defclass picture (cl-prevalence:object-with-id)
  ((title :initarg :title
	  :accessor title)
   (description :initarg :description
		:accessor description)
   (filename :initarg :filename
	     :accessor filename)
   (date :initarg :date
	 :accessor date
	 :initform (local-time:now))
   (publish :initarg :publish
	    :accessor publish
	    :initform t)))

(defmethod print-object ((picture picture) stream)
  (print-unreadable-object (picture stream :type t :identity t)
    (format stream "~A: ~A"
	    (id picture)
	    (title picture))))

(defclass background-image ()
  ((background-image-pathname
    :initarg :pathname
    :accessor background-image-pathname
    :initform (error "Provide the image pathname"))
   (content-type :initarg :content-type
		 :accessor content-type
		 :initform nil)
   (date :initarg :date
	 :accessor background-image-date
	 :initform (get-universal-time))
   (uploaded-filename :initarg :uploaded-filename
		      :accessor uploaded-filename
		      :initform nil)))
;; Prevalence system

(defparameter *system-location*
  (if (getenv "OPENSHIFT_DATA_DIR")
      (ensure-directories-exist (merge-pathnames #p"database/"
						 (pathname (getenv "OPENSHIFT_DATA_DIR"))))
      (ensure-directories-exist (asdf:system-relative-pathname :webapp "database/")))
  "Filesystem location of the prevalence system")

(defparameter *prevalence-system* nil)

(defun init-ukelele-system ()
  (setf *prevalence-system*
	(cl-prevalence:make-prevalence-system *system-location*))
  (unless (cl-prevalence:get-root-object *prevalence-system* :welcome-page)
    (cl-prevalence:execute *prevalence-system*
			   (cl-prevalence:make-transaction
			    'tx-create-welcome-page
			    :title ""
			    :content "")))
  (unless (cl-prevalence:get-root-object *prevalence-system* :contact-page)
    (cl-prevalence:execute *prevalence-system*
			   (cl-prevalence:make-transaction
			    'tx-create-contact-page
			    :content "")))
  (unless (cl-prevalence:get-root-object *prevalence-system* :process-page)
    (cl-prevalence:execute *prevalence-system*
			   (cl-prevalence:make-transaction
			    'tx-create-process-page
			    :content "")))
  (when (not (cl-prevalence:get-root-object *prevalence-system* :id-counter))
    (cl-prevalence:execute-transaction
     (cl-prevalence:tx-create-id-counter *prevalence-system*))))

(defun stop-ukelele-system (&optional (system *prevalence-system*))
  (cl-prevalence:snapshot system)
  (cl-prevalence:close-open-streams system))

;; Operations

;; Background image
(defun tx-update-background-image (system pathname content-type uploaded-filename date)
  (let ((new-background-image
	 (make-instance 'background-image
			:pathname pathname
			:content-type content-type
			:uploaded-filename uploaded-filename
			:date date)))
    (setf (cl-prevalence:get-root-object system :background-image)
	  new-background-image)
    new-background-image))

(defun get-background-image ()
  (cl-prevalence:get-root-object *prevalence-system* :background-image))

(defun update-background-image (&key pathname content-type uploaded-filename (date (get-universal-time)))
  (let ((background-image (cl-prevalence:get-root-object
			   *prevalence-system*
			   :background-image)))
    (when (and background-image (probe-file (background-image-pathname background-image)))
      ;(delete-file (background-image-pathname background-image))
      ))
  (cl-prevalence:execute
   *prevalence-system*
   (cl-prevalence:make-transaction
    'tx-update-background-image
    (or (and (probe-file pathname)
	     (princ-to-string pathname))
	(error "~A does not exist" pathname))
    content-type
    uploaded-filename
    date)))

;; Welcome
(defun tx-update-welcome-page (system title content &optional message button-msg)
  (let ((welcome-page (cl-prevalence:get-root-object system :welcome-page)))
    (setf (title welcome-page) title)
    (setf (content welcome-page) content)
    (setf (message welcome-page) message)
    (setf (button-msg welcome-page) button-msg)
    welcome-page))

(defun get-welcome-page ()
  (cl-prevalence:get-root-object *prevalence-system* :welcome-page))

(defun update-welcome-page (title content message button-msg)
  (cl-prevalence:execute
   *prevalence-system*
   (cl-prevalence:make-transaction 'tx-update-welcome-page title content message button-msg)))

(defun tx-update-marketing (system marketing)
  (setf (cl-prevalence:get-root-object system :marketing) marketing))

(defun update-marketing (marketing)
  (cl-prevalence:execute
   *prevalence-system*
   (cl-prevalence:make-transaction 'tx-update-marketing marketing)))

(defun get-marketing ()
  (cl-prevalence:get-root-object *prevalence-system* :marketing))

;; Contact
(defun tx-update-contact-page (system content)
  (let ((contact-page (cl-prevalence:get-root-object system :contact-page)))
    (setf (content contact-page) content)
    contact-page))

(defun get-contact-page ()
  (cl-prevalence:get-root-object *prevalence-system* :contact-page))

(defun update-contact-page (content)
  (cl-prevalence:execute
   *prevalence-system*
   (cl-prevalence:make-transaction 'tx-update-contact-page content)))

(defun add-slide-picture (&key title description filename)
  (let ((picture
	 (create-picture :title title
			 :description description
			 :filename filename)))
  (cl-prevalence:execute
   *prevalence-system*
   (cl-prevalence:make-transaction 'tx-add-slide-picture (id picture)))
  picture))

(defun tx-add-slide-picture (system picture-id)
  (let ((slides (cl-prevalence:get-root-object system :slides))
	(picture (cl-prevalence:find-object-with-id system 'picture picture-id)))
    (setf (cl-prevalence:get-root-object system :slides)
	  (cons picture slides))))	  

(defun get-slides ()
  (sort 
   (copy-list
    (cl-prevalence:get-root-object *prevalence-system* :slides))
   #'timestamp<
   :key #'date))

(defun delete-slide-picture (picture)
  (let ((picture-id (cl-prevalence:get-id picture)))
    (cl-prevalence:execute-transaction
     (cl-prevalence:tx-delete-object
      *prevalence-system*
      'picture
      picture-id))
    (cl-prevalence:execute-transaction
     (tx-delete-slide-picture *prevalence-system* picture-id))))

(defun tx-delete-slide-picture (system picture-id)
  (setf (cl-prevalence:get-root-object system :slides)
	(remove picture-id  (cl-prevalence:get-root-object system :slides) :key #'id)))

(defun find-slide (id)
  (or
   (find id (get-slides) :key #'id :test #'equalp)
   (error "Slide with id ~A not found" id)))

;; Objects need to be created in transactions in order to be able to be restored
;; from transaction logs
(defun tx-create-welcome-page (system &key title content)
  (setf (cl-prevalence:get-root-object system :welcome-page)
	(make-instance 'welcome-page
		       :title title
		       :content content)))

(defun tx-create-contact-page (system &key content)
  (setf (cl-prevalence:get-root-object system :contact-page)
	(make-instance 'contact-page :content content)))

;; Process

(defun tx-create-process-page (system &key content)
  (setf (cl-prevalence:get-root-object system :process-page)
	(make-instance 'process-page :content content)))

(defun tx-update-process-page (system content)
  (let ((process-page (cl-prevalence:get-root-object system :process-page)))
    (setf (content process-page) content)
    process-page))

(defun get-process-page ()
  (cl-prevalence:get-root-object *prevalence-system* :process-page))

(defun update-process-page (content)
  (cl-prevalence:execute
   *prevalence-system*
   (cl-prevalence:make-transaction 'tx-update-process-page content)))

(defun add-process-picture (&key title description filename date)
  (let ((picture
	 (create-picture :title title
			 :description description
			 :date date
			 :filename filename)))
    (cl-prevalence:execute
     *prevalence-system*
     (cl-prevalence:make-transaction 'tx-add-process-picture (id picture)))
    picture))

(defun tx-add-process-picture (system picture-id)
  (let ((process (cl-prevalence:get-root-object system :process-page))
	(picture (cl-prevalence:find-object-with-id system 'picture picture-id)))
    (push picture (pictures process))))	  

(defun delete-process-picture (picture)
  (cl-prevalence:execute-transaction
   (tx-delete-process-picture *prevalence-system*
			      (cl-prevalence:get-id picture)))
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-delete-object
    *prevalence-system*
    'picture
    (cl-prevalence:get-id picture))))

(defun tx-delete-process-picture (system picture-id)
  (let ((picture (cl-prevalence:find-object-with-id system 'picture picture-id))
	(process (cl-prevalence:get-root-object system :process-page)))
    (setf (pictures process)
	  (remove (cl-prevalence:get-id picture)
		  (pictures process)
		  :key #'cl-prevalence:get-id
		  :test #'equalp))))

(defmethod find-picture ((process-page process-page) picture-id)
  (find picture-id
	(pictures process-page)
	:key #'cl-prevalence:get-id
	:test #'equalp))

;; Works are cl-prevalence are managed objects (subclasses of object-with-id)
(defun get-works ()
  (sort 
   (copy-list
    (cl-prevalence:find-all-objects *prevalence-system* 'work))
   #'timestamp<
   :key #'date))

(defun get-published-works ()
  (remove-if-not #'publish (get-works)))

(defun find-work (id)
  (cl-prevalence:find-object-with-id *prevalence-system* 'work id))

(defun create-work (&key title description short-description date)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-create-object
    *prevalence-system*
    'work
    (list (list 'title title)
	  (list 'description description)
	  (list 'short-description short-description)
	  (list 'date date)))))

(defun update-work (work)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-change-object-slots
    *prevalence-system*
    'work
    (cl-prevalence:get-id work)
    (list (list 'title (title work))
	  (list 'description (description work))
	  (list 'short-description (short-description work))
	  (list 'date (date work))))))

(defun delete-work (work)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-delete-object
    *prevalence-system*
    'work
    (cl-prevalence:get-id work))))

;; Models are cl-prevalence are managed objects (subclasses of object-with-id)
(defun get-models ()
  (sort
   (copy-list
    (cl-prevalence:find-all-objects *prevalence-system* 'model))
   #'timestamp<
   :key #'date))

(defun get-published-models ()
  (remove-if-not #'publish (get-models)))

(defun find-model (id)
  (cl-prevalence:find-object-with-id *prevalence-system* 'model id))

(defun create-model (&key title description short-description date)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-create-object
    *prevalence-system*
    'model
    (list (list 'title title)
	  (list 'description description)
	  (list 'short-description short-description)
	  (list 'date date)))))

(defun update-model (model)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-change-object-slots
    *prevalence-system*
    'model
    (cl-prevalence:get-id model)
    (list (list 'title (title model))
	  (list 'description (description model))
	  (list 'short-description (short-description model))
	  (list 'date (date model))))))

(defun delete-model (model)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-delete-object
    *prevalence-system*
    'model
    (cl-prevalence:get-id model))))

;; Pictures
(defun create-picture (&key title description filename date)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-create-object
    *prevalence-system*
    'picture
    (list (list 'title title)
	  (list 'description description)
	  (list 'date date)
	  (list 'filename filename)))))

(defun update-picture (picture)
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-change-object-slots
    *prevalence-system*
    'picture
    (cl-prevalence:get-id picture)
    (list (list 'title (title picture))
	  (list 'description (description picture))
	  (list 'filename (filename picture))
	  (list 'date (date picture))))))

;; Works pictures

(defun delete-picture (work picture)
  (cl-prevalence:execute-transaction
   (tx-remove-picture *prevalence-system*
		      (cl-prevalence:get-id work)
		      (cl-prevalence:get-id picture)))
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-delete-object
    *prevalence-system*
    'picture
    (cl-prevalence:get-id picture))))

(defun add-picture (work picture)
  (cl-prevalence:execute-transaction
   (tx-add-picture *prevalence-system*
		   (cl-prevalence:get-id work)
		   (cl-prevalence:get-id picture))))

(defun tx-add-picture (system work-id picture-id)
  (let ((picture (cl-prevalence:find-object-with-id system 'picture picture-id))
	(work (cl-prevalence:find-object-with-id system 'work work-id)))
      (push picture (pictures work))))

(defun tx-remove-picture (system work-id picture-id)
  (let ((picture (cl-prevalence:find-object-with-id system 'picture picture-id))
	(work (cl-prevalence:find-object-with-id system 'work work-id)))
    
    (setf (pictures work)
	  (remove (cl-prevalence:get-id picture)
		  (pictures work)
		  :key #'cl-prevalence:get-id
		  :test #'equalp))))

(defun move-picture (work picture direction)
  (cl-prevalence:execute-transaction
   (tx-move-picture *prevalence-system*
		    (cl-prevalence:get-id work)
		    (cl-prevalence:get-id picture)
		    direction)))

(defun tx-move-picture (system work-id picture-id direction)
  (let* ((work (cl-prevalence:find-object-with-id system 'work work-id))
	 (pictures (pictures work)))
    (setf (pictures work)
	  (move picture-id pictures direction
		:key #'cl-prevalence:get-id
		:test #'equalp))))

(defun move (thing list direction &key (key #'identity)
				    (test #'eql))
  (%move thing list direction key test))

(defun %move (thing list direction key test &optional result)
  (cond
    ((null list)
     result)
    ((funcall test thing (funcall key (car list)))
      ;; Element found, move it
      (ecase direction
	(:forward (remove-if #'null
			     (append result
				     (list
				      (cadr list)
				      (car list))
				     (cddr list))))
	(:backward (remove-if #'null
			      (append (butlast result)
				      (list
				       (car list)
				       (car (last result)))
				      (rest list))))))
    (t (%move thing (rest list) direction key test
	      (append result (list (car list)))))))

;; Models pictures

(defun delete-picture-from-model (model picture)
  (cl-prevalence:execute-transaction
   (tx-remove-picture-from-model *prevalence-system*
				 (cl-prevalence:get-id model)
				 (cl-prevalence:get-id picture)))
  (cl-prevalence:execute-transaction
   (cl-prevalence:tx-delete-object
    *prevalence-system*
    'picture
    (cl-prevalence:get-id picture))))

(defun add-picture-to-model (model picture)
  (cl-prevalence:execute-transaction
   (tx-add-picture-to-model *prevalence-system*
			    (cl-prevalence:get-id model)
			    (cl-prevalence:get-id picture))))

(defun tx-add-picture-to-model (system model-id picture-id)
  (let ((picture (cl-prevalence:find-object-with-id system 'picture picture-id))
	(model (cl-prevalence:find-object-with-id system 'model model-id)))
      (push picture (pictures model))))

(defun tx-remove-picture-from-model (system model-id picture-id)
  (let ((picture (cl-prevalence:find-object-with-id system 'picture picture-id))
	(model (cl-prevalence:find-object-with-id system 'model model-id)))
    
    (setf (pictures model)
	  (remove (cl-prevalence:get-id picture)
		  (pictures model)
		  :key #'cl-prevalence:get-id
		  :test #'equalp))))

(defmethod find-picture ((model model) picture-id)
  (find picture-id
	(pictures model)
	:key #'cl-prevalence:get-id
	:test #'equalp))

(defmethod find-picture ((work work) picture-id)
  (find picture-id
	(pictures work)
	:key #'cl-prevalence:get-id
	:test #'equalp))
